# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require evince

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc
    djvu       [[ description = [ Enable support for DjVu format ] ]]
    dvi        [[ description = [ Enable support for DVI format ] ]]
    gspell     [[ description = [ Enable support for spell-checking ] ]]
    gstreamer  [[ description = [ Enable support for showing videos ] ]]
    keyring    [[ description = [ Use GNOME Keyring to store passwords ] ]]
    nautilus   [[ description = [ Build Nautilus extensions ] ]]
    postscript [[ description = [ Enable support for PostScript format ] ]]
    tiff       [[ description = [ Enable support for multi-page TIFF format ] ]]
    xps        [[ description = [ enable support for XPS documents ] ]]
"

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gi-docgen[>=2021.1] )
    build+run:
        app-arch/libarchive[>=3.6.0]
        app-text/poppler[>=22.02.0][cairo][glib]
        dev-libs/atk
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libhandy:1[>=1.5.0]
        dev-libs/libxml2:2.0[>=2.5.0]
        gnome-desktop/adwaita-icon-theme[>=2.17.1]
        gnome-desktop/gnome-desktop:4[legacy]
        gnome-desktop/gsettings-desktop-schemas
        sys-libs/zlib
        x11-libs/cairo[>=1.10.0]
        x11-libs/gdk-pixbuf:2.0[>=2.40.0]
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection?]
        x11-libs/pango
        dvi? (
            app-text/libspectre[>=0.2.0]
            dev-libs/kpathsea
            media-libs/t1lib
        )
        djvu? ( app-text/djvu[>=3.5.22] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.0] )
        gspell? ( gnome-desktop/gspell:1[>=1.6.0] )
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        keyring? ( dev-libs/libsecret:1[>=0.5] )
        nautilus? ( gnome-desktop/nautilus[>=3.28.0] )
        postscript? ( app-text/libspectre[>=0.2.0] )
        tiff? ( media-libs/tiff[>=4.0] )
        xps? ( dev-libs/libgxps[>=0.2.1] )
    run:
        x11-misc/shared-mime-info
    suggestion:
        gnome-desktop/gvfs [[ description = [ Required for bookmark functionality ] ]]
"

# Requires X and dogtail
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    "-Dsystemduserunitdir=${SYSTEMDUSERUNITDIR}"

    '-Dpdf=enabled'
    '-Dcomics=enabled'
    '-Ddbus=true'
    '-Dviewer=true'

    # Unpackaged
    '-Dinternal_synctex=true'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'nautilus'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'djvu'
    'dvi'
    'gspell'
    'gstreamer multimedia'
    'keyring'
    'postscript ps'
    'tiff'
    'xps'
)

