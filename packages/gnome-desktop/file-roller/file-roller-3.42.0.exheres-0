# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Purpose License v2

require file-roller

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    archive [[ description = [ add support for archive formats that libarchive supports ] ]]
    libnotify [[ description = [ display completion notifications ] ]]
    nautilus-extension [[ description = [ build nautilus context menu actions ] ]]
"

DEPENDENCIES="
    build:
        dev-util/itstool
        virtual/pkg-config
    build+run:
        core/json-glib[>=0.14.0]
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libhandy:1[>=1.5.0]
        virtual/cpio
        x11-libs/gtk+:3[>=3.22.0]
        archive? ( app-arch/libarchive[>=3.0.0] )
        libnotify? ( x11-libs/libnotify[>=0.4.3] )
        nautilus-extension? ( gnome-desktop/nautilus[>=3.28.0] )
    suggestion:
        app-arch/unzip [[
            description = [ ${PN} can only use InfoZIP unzip to extract .zip files ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Drun-in-place=false
    -Dpackagekit=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'archive libarchive'
    'libnotify notification'
    'nautilus-extension nautilus-actions'
)

