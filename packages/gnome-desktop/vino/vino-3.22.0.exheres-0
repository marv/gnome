# Copyright 2010 Paul Mulders <info@mld.demon.nl>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
require systemd-service

SUMMARY="GNOME Remote Desktop Server"
HOMEPAGE="http://live.gnome.org/Vino/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="avahi gcrypt gnutls libnotify
    systemd [[ description = [ Install systemd user unit for vino-server ] ]]
    telepathy [[ description = [ Support remote desktop sessions using telepathy tubes ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.16]
        dev-util/intltool[>=0.35.0]
    build+run:
        dev-libs/glib:2[>=2.32.0]
        dev-libs/libsecret:1
        sys-apps/dbus
        x11-libs/gtk+:3[>=3.0.0]
        x11-libs/libICE
        x11-libs/libnotify[>=0.7.0]
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXtst
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=8] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        avahi? ( net-dns/avahi[>=0.6] )
        gcrypt? ( dev-libs/libgcrypt[>=1.1.90] )
        gnutls? ( dev-libs/gnutls[>=1.0.0] )
        telepathy? (
            net-im/telepathy-glib[>=0.11.6]
            dev-libs/dbus-glib:1
        )
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--with-secret'
    '--with-jpeg'
    '--with-zlib'
    '--enable-ipv6'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'avahi'
    'gcrypt'
    'gnutls'
    'telepathy'
    "systemd systemduserunitdir=${SYSTEMDUSERUNITDIR}"
)

