# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose Licence v2

require gnome.org [ suffix=tar.xz ] gsettings
require gtk-icon-cache freedesktop-desktop freedesktop-mime

SUMMARY="Help browser for the GNOME desktop"
HOMEPAGE="https://projects.gnome.org/yelp"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( linguas:
        af am an ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs cy da de dz el
        en_CA en_GB eo es et eu fa fi fr fur ga gd gl gu he hi hr hu id is it ja ka kk km kn ko ks
        ku ky la li lt lv mai mg mk ml mn mr ms nb nds ne nl nn nso oc or pa pl ps pt pt_BR ro ru rw
        si sk sl sq sr sr@latin sv ta te tg th tr ug uk uz uz@cyrillic vi wa xh zh_CN zh_HK zh_TW zu
    )
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.1.2
        dev-libs/appstream-glib
        dev-util/itstool[>=1.2.0]
        sys-devel/gettext[>=0.19.8]
    build+run:
        app-arch/xz[>=4.9]
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.67.4]
        dev-libs/libhandy:1[>=1.5.0]
        dev-libs/libxml2:2.0[>=2.6.5]
        dev-libs/libxslt[>=1.1.4]
        gnome-desktop/yelp-xsl[>=41.0]
        net-libs/webkit:4.1
        x11-libs/gtk+:3[>=3.13.3]
    run:
        gnome-desktop/adwaita-icon-theme [[ note = [ Provides application icon ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-appstream-util
    --enable-bz2
    --enable-lzma
)

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

