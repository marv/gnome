# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ] gsettings freedesktop-desktop gtk-icon-cache
require meson

SUMMARY="GTK+ based Web Browser"
HOMEPAGE="https://projects.gnome.org/epiphany"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    (
        providers:
            soup2
            soup3
    ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        virtual/pkg-config[>=0.20]
    build+run:
        app-arch/libarchive
        app-text/iso-codes[>=0.35]
        core/json-glib[>=1.6.0]
        dev-db/sqlite:3[>=3.22]
        dev-libs/glib:2[>=2.67.4]
        dev-libs/icu:=[>=4.6]
        dev-libs/libdazzle:1.0[>=3.37.1]
        dev-libs/libhandy:1[>=1.5.0]
        dev-libs/libsecret:1[>=0.19.0]
        dev-libs/libxml2:2.0[>=2.6.12]
        dev-libs/nettle:=[>=3.4]
        gnome-desktop/gcr[>=3.5.5]
        gnome-desktop/gnome-desktop:4[>=2.91.2][legacy]
        gnome-desktop/gsettings-desktop-schemas
        x11-libs/cairo[>=1.2]
        x11-libs/gdk-pixbuf:2.0[>=2.36.5]
        x11-libs/gtk+:3[>=3.24.0]
        x11-libs/libnotify[>=0.5.1]
        providers:soup2? (
            gnome-desktop/libsoup:2.4[>=2.48.0]
            net-libs/webkit:4.0[>=2.33.2]
        )
        providers:soup3? (
            gnome-desktop/libsoup:3.0[>=2.99.4]
            net-libs/webkit:4.1[>=2.33.2]
        )
    run:
        gnome-desktop/adwaita-icon-theme
    suggestion:
        net-apps/NetworkManager[>=0.8.9997] [[
            description = [ Offline mode autodetection ]
        ]]
"

# requires X
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dunit_tests=disabled'
    '-Dlibportal=disabled'
    '-Dnetwork_tests=disabled'
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'providers:soup2 soup2'
)

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

